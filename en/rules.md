---
title: _Hauptallee Hügel Homies_ Flunkyball Standard Team Rules
subtitle: English v0.1 Draft
author:
 - Lukas Grassauer
 - H. M. Grünsteidl
 - M. Hauser
 - Ha. Ca. Kuk.
 - L. Vavra
 - and other members of the HHH-Flunky-Rule-committee
---

# Preliminaries

## Disambiguation

### Match

+ A match is a single game of Flunkyball.

### Throw

+ A throw also called a _turn_ or _ply_, is one throw of a team.

### Round

+ A round concludes when both _throws_ of each team are done. A round always
  starts with the starting team and ends with the other team.

### Lap

+ A lap concludes after every player still standing, threw. A lap does not
  necessarily start with the same team.

## Drink Unit

+ To calculate the drink unit, multiply the volume of the container by 2 and
  multiply that number by the relative alcohol volume percent times 100[^1].

+ $D = V * 2 * A * 100$
  - Where $D$ is the drink unit
  - $V$ is the volume of the container
  - $A$ is the alcohol volume percent

+ Examples:
  - $0.5$L Beer with $4.2$% alc. $\rightarrow D = 0.5 * 2 * 0.042 * 100 = 4.2$
  - $1$L Red wine with $10.5$% alc. $\rightarrow D = 1 * 2 * 0.105 * 100 = 21$
  - $0.02$L Vodka with $40$% alc. $\rightarrow D = 0.02 * 2 * 0.40 * 100 = 1.6$

+ The standard drink unit is the most common of all drink units in a game. If
  all drink units are unique the median is the standard.

[^1]: A 0.5L container's drink unit is exactly its relative alcohol volume percent.

## Decider

+ A decider is a mini-game where each team picks a champion by vote. If there is
  a tie, a random player from the tied players will be determined as the champion.

+ The champions of each team now need to determine a way to decide who wins the
  decider. If there is no consensus, a _single_ victory of _Rock, Paper,
  Scissors_ determines the decider.


# Players

+ Flunkyball is a team game, which requires at least two people, therefore at
  least one person per team, however there is no limit on the players.

+ The teams may only differ one in size.

+ How the teams are determined is to be decided amongst the competitors. Should
  there be no consensus on how to split the athletes in teams, they ought to be
  randomised.

# Items

## The Drink
+ Each player requires at least one of the _same kind_ of alcoholic beverage,
  preferably of _same brand_, while it is vital that they are in the _same kind_
  of _unopened_ container.
    - In case of cans it is explicitly allowed to modify the container after
      opening. However, it may not leak while it is not drank from.
    - Should the drink unit of ones drink be less or equal than the three
      quarters of the standard, the player has to take additional containers
      until the newly calculated drink unit is higher than the standard. If it
      is higher than the standard then no further action is necessary.

+ Both teams need to accept each drink, even if they fall under same kind of
  alcoholic beverage. Should there be no consensus another kind of drink has to
  be taken.


## The Target

- An object, where it can be easily determined if it is fallen over, with
  enough weight that it only falls over once it was hit by the throwing object.

- Should this object be damaged during a game, beyond a point that these
  properties are no longer given, it is to be exchanged, at the start of a
  round, for an item which fulfils these requirements. However, should no other
  suitable object exist, the game must be finished with the original target.

- Both teams have to agree on the target. If they do not, a decider is played
  and the winner may choose a rule abiding object.


## The Throwing Object

- An object, which should not be heavier than the target, however at least a
  quarter of the weight of the target. Note that this is more of a guideline, if
  it is not possible anything can be the throwing object.

- Should the throwing object be damaged beyond a point that it can no longer tip
  over the target fairly it is to be exchanged immediately, if possible.

- Both teams have to agree on the throwing object. If they do not, a decider is
  played and the winner may choose a rule abiding object.


# The Pitch

+ A Flunkyball pitch should be at least 10 meters and should be at least as level that
  the target does not fall over by itself. It consists of the centre, the
  guidance lines and the beer lines.

+ The centre is a point, preferably marked, from which in opposite directions in
  straight lines - the guidance lines - of at least 5 meters, the beer lines
  start orthogonally. The centre should be marked but this is not required.
  - The target is placed on the centre point.

+ The guidance lines are two lines which start from the centre and are at least
  5 meters long. They should be marked but this is not required.
  - If there is no measuring tape available use a foot length as standard. This
    table gives an overview for 5 meter guidance lines:

    | EU Shoe Size | cm   | "feet" |
    |--------------|------|--------|
    |           39 | 26.5 |     19 |
    |           42 |   30 |     16 |

+ The beer lines are straight lines which are not limited in length. The distance
  from the centre to their crossing should be at least be twice of the guidance
  line lengths.
  - The drinks are placed on the beer lines.

+ If there are markings they have to be accepted by both teams.


# The Game

## Preparation

+ Both teams need to agree on these rules.

+ The pitch needs to be laid out.

+ A decider is played, the winner's team is the first team and the loser's the
  second team.

+ The second team chooses their side.

+ Once both teams indicate that they are ready the containers are opened.
  - Opening the container too early is considered a _light_ rule violation.

## The Play

+ The game has three distinct phases, which overlap:
  - Throwing the throwing object (also called _YEET IT_)
  - Running and restoring the pitch (also called _SKEET IT_)
  - Drinking from the drinking containers (also called _DELETE IT_)

+ The team throwing is called offence.

+ The team restoring the pitch is called defence.


### Penalties

+ There are rule violations in different severities -- light, medium, high, severe.
  - Light rule violations do not benefit or only marginally benefits the
    offending player.
  - Medium rule violations benefit the offending player or their team, or are
    cases of foul sportsmanship.
  - High rule violations greatly benefit offending player or their team, or are
    cases of foul sportsmanship.
  - Severe rule violations renders the game unfair, or are severe cases of foul
    sportsmanship.

+ Should it be impossible to impose a penalty, then the next higher
  possible penalty is to be sentenced. The penalised team can always choose to
  take a higher penalty.

On light rule violations
: _Skip the next delete_. The penalised player(s) are exempt from being allowed
  to drink the next time their team successfully tips over the target during the
  _YEET IT_ phase. This penalty cannot be imposed on a player if they are the
  only remaining player in a team. This penalty does not stack, this means that
  once this penalty is imposed on a player it cannot be sentenced on this player
  again.

On medium rule violations
: _Skip the next throw_. The next time the penalised player(s) team is the
  offence they return into the defence. This penalty does not stack, this means
  that once this penalty is imposed on a team it can not be sentenced again.

On high rule violations
: _Penalty drink unit_. The penalised players(s) have to take at least one
  standard drink unit. Taking a drink unit does not require opening it. If there
  are no drinks left, this penalty cannot be imposed.

On severe rule violations
: _Forfeit the game_. The penalised team immediately loses the match.


### Yeet It

+ This phase consists of the offence throwing the throwing object, to allow them
  to tip over the target, or possibly the defence's drinking containers.

+ Every player in the team has to throw once a lap and therefore, is only
  allowed to throw again once the lap concludes. Every player throwing more than
  once a lap is committing a _light_ rule violation.

+ Throwing has to be done in such a way that the throwing object is above the
  elbow when it is released from the hand of the thrower. Violating
  this rule is a _medium_ rule violation.

+ _Every_ player in the offence has to stand behind their teams beer line.
  Violating this rule is a _medium_ rule violation.

+ Although there is no time limit when to throw the object, please conclude your
  throw within 60 seconds.


### Skeet It

+ This phase consists of the defence repositioning the target on the centre
  point, returning behind the beerline and thereby restoring the pitch. If the
  offence did not tip over the target the pitch is considered restored by
  default.

+ Only one player is allowed to restore the pitch. Every additional player
  crossing the beerline, except for defending the drinks, is considered a
  _light_ rule violation.

+ The player restoring to the pitch is allowed to cross the beer line once the
  throwing object is released from the throwers hand. Violating this rule is
  considered a _medium_ rule violation.

+ It might happen that the offence throws the throwing object in such a way,
  that it tips over the defence's drinks. This is allowed (and encouraged) and
  results in a _penalty drink unit_ for the defender. Notice that this is not a
  rule violation.

+ The other players are allowed to defend the drinks once the throwing object
  leaves the hand of the thrower, by putting parts of their body before the
  drinks, this is not considered stepping or being over the beer line. However,
  defending too early is considered a _light_ rule violation.

+ However touching the drinking container is not allowed, except before the
  offence throw or if a drinking container is tipped over. Moreover, in this case
  it has to be repositioned immediately otherwise it is a _severe_ rule
  violation. Illegally touching the drinking container is considered a _light_
  rule violation.

+ The target does not have to be placed on the exact centre point. However, the
  distance to the centre point may not exceed 5% of the pitch length (measured
  against the guidance line). Should it be off the target is to be repositioned
  and the skeeter committed a _light_ rule violation. If a team objects it is
  allowed to ask for a measurement.
  - If the team that asked for the measurement did so three times throughout the
    game without the other team actually violating the 5% discrepancy is
    considered foul sportsmanship and thereby a _medium_ rule violation.

+ After restoring the pitch, the defence must scream a stop word; "Stop" or (in
  German) "Stopp". Screaming the stop word too early is a _medium_ rule
  violation, after the first offence and a _high_ rule violation on every
  consecutive case.


### Delete It

+ This phase consists of the offence drinking from their drink units, if and
  only if they tipped over the target in the _SKEET IT_ phase.

+ Every player not penalised with _Skip next delete_ (given after a _light_ rule
  violation) is eligible to drink from the drinking container (delete,
  deleting), however drinking is not mandatory.

+ The container may only leave the ground after the target is fully tipped over.
  Violating this rule is a _light_ rule violation.

+ After the stop word was audibly screamed the offence has to stop to drink,
  otherwise continuing to drink it is considered a _medium_ rule violation.

+ If a player has to spit out parts of their drink or puke, they commit a _high_
  rule violation. However, if the player pukes only in their mouth it is still
  legal.

+ Drinking from another player's drinking container is a _high_ rule violation.

+ To proof a drink is empty, the player has to tilt it in such a way that the
  opening of the drink points downwards, preferably over their own head, for
  three consecutive seconds. Should during those three seconds be a continuous
  stream of liquid, this is considered a _medium_ rule violation. For
  clarification: Drops or some amount of foam are within the rules.

+ If it has been proven that all drinks are empty the player is done. Note that
  this implies that a player only ever finishes on their delete.

## Winning The Game

+ Flunkyball has always a winner and a loser, never ties.

+ A game is called finished if:
  - One team finishes all their players. This team is considered the winning team.
  - One team receives a _Forfeit the game_ (on _severe_ rule violations)
    penalty. The other team is considered the winning team.

+ Should both teams be penalised with _Forfeit the game_ in the same throw, the
  game is not considered finished and the game continues.

+ Should through any circumstance the game not be finished or it is not able to
  be finished, the team with less remaining players wins.

+ Should both have the same amount of players left, the team of the first
  finished player wins.


# Miscellaneous Rules

+ These rules are exhaustive per definition. Should there be a case which is not
  covered by this rule-set, the match cannot be deemed an official _Hauptallee
  Hügel Homies_ game.

+ If any liquid spills (except when proving a container to be empty) this is
  considered a _high_ rule violation.

+ Calling timeouts to clarify rules or resolve problems is legal. The game is
  resumed after both teams confirm they are ready.
  - Spilling or drinking from drinks during timeouts are still punished
    according to the rules.
  - Not honouring timeouts, i.e. throwing while timeout, is foul sportsmanship
    and therefore a _medium_ rule violation.

+ After accepting something it is part of the game and cannot be changed
  subsequently.

+ Drink responsibly.

+ If there is no referee, the game relies on self checking.

+ Play safe and play fair.

+ There are no subsequent penalties.

+ Enforcement of rules are an obligation for the claimants, not an obligation to
  fulfil by the offender.

+ Leaving the pitch during the game is only allowed if all remaining players
  agree. Going without permission is a _light_ rule violation.

+ Sidedrinks, i.e. drinks not involved with the official game, are _always_
  legal.
