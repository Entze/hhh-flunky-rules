all: public/index.html public/en/index.html public/en/rules.pdf

%.pdf: %.md
	pandoc --metadata date="$(shell date -u '+%Y-%m-%d')" --from markdown --to latex --standalone -V papersize:a4 $^ --output $@

%.html: %.md
	pandoc --metadata date="$(shell date -u '+%Y-%m-%d')" --from markdown --to html --mathml --standalone $< --output $@

public/:
	mkdir -p $@

public/en/: | public/
	mkdir -p $@

public/de/: | public/
	mkdir -p $@

public/index.html: public/en/index.html
	cp $< $@

public/%/index.html: %/rules.md | public/%/
	pandoc --metadata date="$(shell date -u '+%Y-%m-%d')" --from markdown --to html --mathml --standalone $< --output $@

public/%/rules.pdf: %/rules.md | public/%/
	pandoc --metadata date="$(shell date -u '+%Y-%m-%d')" --from markdown --to latex --standalone -V papersize:a4 $^ --output $@

clean:
	$(RM) -r public/

.PHONY: all clean
